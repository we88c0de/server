# From: https://about.gitlab.com/2017/09/21/how-to-create-ci-cd-pipeline-with-autodeploy-to-kubernetes-using-gitlab-and-helm/

cache:
  untracked: true
  key: "$CI_BUILD_REF_NAME"
  paths:
    - vendor/

variables:
  REPO_DIR: gitlab.com/elixxir
  REPO_NAME: server
  DOCKER_IMAGE: elixxirlabs/cuda-go:latest
  MIN_CODE_COVERAGE: "81.0"

before_script:
  ##
  ## Go Setup
  ##
  - go version || echo "Go executable not found."
  - echo $CI_BUILD_REF
  - echo $CI_PROJECT_DIR
  - echo $PWD
  - echo $USER
  - eval $(ssh-agent -s)
  - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
  - mkdir -p ~/.ssh
  - chmod 700 ~/.ssh
  - ssh-keyscan -t rsa gitlab.com > ~/.ssh/known_hosts
  - git config --global url."git@gitlab.com:".insteadOf "https://gitlab.com/"
  - export PATH=$HOME/go/bin:$PATH
  - export GOPRIVATE=gitlab.com/elixxir/*,gitlab.com/xx_network/*

stages:
  - build
  - trigger_integration
  - trigger_release_integration

build:
  stage: build
  image: $DOCKER_IMAGE
  tags:
    - gpu
  script:
    - git clean -ffdx
    # Because gpumaths is no longer one repository, let's get the native library with the release branch now
    # Branch switching for the native library should be added shortly
    - GPUMATHS_VERSION=release
    - echo $GPUMATHS_VERSION
    - rm -fr gpumathsnative
    - git clone -b $GPUMATHS_VERSION git@gitlab.com:elixxir/gpumathsnative.git
    - pushd gpumathsnative
    - git clean -ffdx
    - pushd cgbnBindings/powm
    - make turing
    - make install
    - ls
    - ls /opt/xxnetwork/lib
    - popd
    - popd
    - go mod vendor -v
    - go build ./...
    - go mod tidy

    - cat /usr/local/cuda/version.txt
    - nvidia-smi

    - mkdir -p testdata

    # Test coverage
    - go-acc --covermode atomic --output testdata/coverage.out ./... -- -parallel 1 -v
    # Test coverage with gpu extension (note that this doesn't overwrite existing coverage)
    - go-acc --covermode atomic --output testdata/coverage.out ./... -- -parallel 1 -v -tags gpu
    # Exclude cmd from test coverage as it is command line related tooling
    # Exclude database files from test coverage due to lack of Postgre
    # Exclude newRound because of lots of side effects
    # Exclude testutil patterns because they are not used in binary outside tests
    - grep -v -e cmd -e userDatabase.go -e newRound.go -e testUtil -e testutil testdata/coverage.out > testdata/coverage-real.out
    # Get coverage data
    - go tool cover -func=testdata/coverage-real.out
    - go tool cover -html=testdata/coverage-real.out -o testdata/coverage.html

    # Benchmarking
    #- go test -bench=BenchmarkPrecomp -cpuprofile=testdata/precomp.cpu -memprofile=testdata/precomp.mem -short main_benchmarks_test.go
    #- go tool pprof -png main.test testdata/precomp.cpu > testdata/precomp-cpu.png
    #- go tool pprof -png main.test testdata/precomp.mem > testdata/precomp-mem.png
    #- go test -bench=BenchmarkRealtime -cpuprofile=testdata/realtime.cpu -memprofile=testdata/realtime.mem -short main_benchmarks_test.go
    #- go tool pprof -png main.test testdata/realtime.cpu > testdata/realtime-cpu.png
    #- go tool pprof -png main.test testdata/realtime.mem > testdata/realtime-mem.png

    # Test Coverage Check
    - go tool cover -func=testdata/coverage-real.out | grep "total:" | awk '{print $3}' | sed 's/\%//g' > testdata/coverage-percentage.txt
    - export CODE_CHECK=$(echo "$(cat testdata/coverage-percentage.txt) >= $MIN_CODE_COVERAGE" | bc -l)
    - (if [ "$CODE_CHECK" == "1" ]; then echo "Minimum coverage of $MIN_CODE_COVERAGE succeeded"; else echo "Minimum coverage of $MIN_CODE_COVERAGE failed"; exit 1; fi);

    - mkdir -p release
    - GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -ldflags '-w -s' ./...
    - GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -ldflags '-w -s' -o release/server.linux64 main.go
    - GOOS=windows GOARCH=amd64 CGO_ENABLED=0 go build -ldflags '-w -s' -o release/server.win64 main.go
    - GOOS=windows GOARCH=386 CGO_ENABLED=0 go build -ldflags '-w -s' -o release/server.win32 main.go
    - GOOS=darwin GOARCH=amd64 CGO_ENABLED=0 go build -ldflags '-w -s' -o release/server.darwin64 main.go
    - cp /opt/xxnetwork/lib/* release/
    - cp /opt/xxnetwork/include/* release/
    - GOOS=linux GOARCH=amd64 CGO_ENABLED=1 go build -tags gpu -ldflags '-w -s -L /opt/xxnetwork/lib' -o release/server-cuda.linux64 main.go
  artifacts:
    paths:
      - vendor/
      - testdata/
      - gpumathsnative/
      - release/

tag_and_trigger:
  stage: trigger_integration
  only:
    - master
  image: $DOCKER_IMAGE
  script:
    - git remote add origin_tags git@gitlab.com:elixxir/server.git || true
    - git remote set-url origin_tags git@gitlab.com:elixxir/server.git || true
    - git tag $(./release/server.linux64 version | grep "xx network Server v"| cut -d ' ' -f4) -f
    - git push origin_tags -f --tags
    - "curl -X POST -F token=3cd593ad56ec017e30254c9ec6c0ab -F ref=master https://gitlab.com/api/v4/projects/5615854/trigger/pipeline"

trigger_release_integration:
  stage: trigger_release_integration
  script:
    - "curl -X POST -F token=e34aa19ef1530e579c5d590873d3c6 -F ref=release -F \"variables[CLIENT_ID]=release\" -F \"variables[GATEWAY_ID]=release\" -F \"variables[REGISTRATION_ID]=release\" -F \"variables[SERVER_ID]=release\" -F \"variables[UDB_ID]=release\" https://gitlab.com/api/v4/projects/5615854/trigger/pipeline"
  only:
    - release
